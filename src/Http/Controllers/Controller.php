<?php

namespace Solle\PcbMessage\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function response($code = 200,$message = 'ok',$data = []){
        $response = [
            "code"          => $code,
            "message"       => $message,
            "data"          => $data
        ];
        return response()->json($response);
    }

    public function responseFail($message = "fail",$data = []){
        return $this->response(500,$message,$data);
    }

    public function responseSuccess($data = [],$message = "ok"){
        return $this->response(200,$message,$data);
    }

}
