<?php

namespace Solle\PcbMessage\Http\Middleware;

use Illuminate\Support\Facades\Crypt;
use PcbMessage;

class SendMessageMiddleware
{
    public function handle($request, $next){
        $token  = $request->bearerToken();
        $info   = PcbMessage::parseFeedbackKey($token);
        if(empty($info)){
            return response()->json(["code" => 200,"message" => "illegal request"]);
        }
        $request->feedback_object = $info;
        return $next($request);
    }
}
