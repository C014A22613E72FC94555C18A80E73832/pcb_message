<?php

namespace Solle\PcbMessage;

class PcbMessageProvider extends \Illuminate\Support\ServiceProvider
{
    public function boot(){
        //加载modules路由
        $this->loadRoutesFrom(__DIR__ . '/routes/routes.php');
        //加载modules视图
        $this->loadViewsFrom(__DIR__ . '/resources/views', 'pcbmessage');
    }
}
