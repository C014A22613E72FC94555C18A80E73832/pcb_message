<?php

namespace Solle\PcbMessage;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;
use Utils\SystemVisitor;

class Message
{
    public static function newById($id,$user_id){
        $message    = new self();
        $log        = DB::table("pcb_message")->find($id);
        if(!empty($log)){
            if($log->is_admin_sender){
                //客服
                $message->avatar = "/resources/images/kf.png";
            }else{
                $message->avatar = DB::table("pcb_accounts")->where("id",'=',$log->sender_id)->value("face");
                if(empty($message->avatar)){
                    $message->avatar = "/resources/images/people.png";
                }
            }
            $message->id       = $id;
            $message->username = $log->sender;
            $message->create_time = $log->create_time;
            $message->content = json_decode($log->content,true);
            $message->is_send = false;
            if(SystemVisitor::isAdmin() && $log->is_admin_sender && (int)$log->sender_id === (int)$user_id){
                $message->is_send = true;
            }
            if(SystemVisitor::isUser() && !$log->is_admin_sender && (int)$log->sender_id === (int)$user_id){
                $message->is_send = true;
            }
        }
        return $message;
    }

    public static function newList($list,$user_id,$stream = ""){
        if(empty($list)){
            return [];
        }
        $result = [];
        $userAvatar = [];
        $reads = [];
        foreach($list as $l){
            $message = new self();
            if($l->is_admin_sender){
                //客服
                $message->avatar = "/resources/images/kf.png";
            }else{
                if(empty($userAvatar[$l->sender_id])){
                    $userAvatar[$l->sender_id] = DB::table("pcb_accounts")->where("id",'=',$l->sender_id)->value("face");
                    if(empty($userAvatar[$l->sender_id])){
                        $userAvatar[$l->sender_id] = "/resources/images/people.png";
                    }
                }
                $message->avatar = $userAvatar[$l->sender_id];
            }
            if($l->receiver_id == $user_id && !$l->is_read){
                if(SystemVisitor::isAdmin() && $l->is_admin_receiver){
                    array_push($reads,$l->id);
                }
                if(SystemVisitor::isUser() && !$l->is_admin_receiver){
                    array_push($reads,$l->id);
                }
            }
            $message->id            = $l->id;
            $message->username      = $l->sender;
            $message->create_time   = $l->create_time;
            $message->content       = json_decode($l->content,true);
            $message->is_send       = false;
            if(SystemVisitor::isAdmin() && $l->is_admin_sender && (int)$l->sender_id === (int)$user_id){
                $message->is_send = true;
            }
            if(SystemVisitor::isUser() && !$l->is_admin_sender && (int)$l->sender_id === (int)$user_id){
                $message->is_send = true;
            }
            array_push($result,$message);
            if($stream){
                MessagePushRecord::record($l->session,SystemVisitor::getRole(),SystemVisitor::getId(),$l->id,$l->create_time);
            }
        }
        if(!empty($reads)){
            DB::table("pcb_message")->whereIn("id",$reads)->update(["is_read" => 1]);
        }
        return $result;
    }

}
