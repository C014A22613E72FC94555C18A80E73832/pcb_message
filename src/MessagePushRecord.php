<?php

namespace Solle\PcbMessage;

use Illuminate\Support\Facades\Redis;
use Utils\SystemVisitor;

class MessagePushRecord
{
    public static function record($session,$role,$user_id,$max_id,$max_timestamp){
        Redis::set(sprintf("%s_%s_%d_SSE_MAX_TIMESTAMP",
            $session
            ,$role
            ,$user_id)
            ,$max_timestamp);
        Redis::set(sprintf("%s_%s_%d_SSE_MAX_ID",
            $session
            ,$role
            ,$user_id)
            ,$max_id);
    }

    public static function getRecord($session,$role,$user_id){
        $max_timestamp = Redis::get(sprintf("%s_%s_%d_SSE_MAX_TIMESTAMP",
                $session
                ,$role
                ,$user_id));
        $max_id = Redis::get(sprintf("%s_%s_%d_SSE_MAX_ID",
                $session
                ,$role
                ,$user_id));
        return [$max_id,$max_timestamp];
    }

}
