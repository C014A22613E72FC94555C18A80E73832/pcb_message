<?php

use Illuminate\Support\Facades\Route;

Route::prefix("/message")->group(function(){
    Route::get("/chat",[\Solle\PcbMessage\Http\Controllers\Message::class,"chat"]);
    Route::post("/send",[\Solle\PcbMessage\Http\Controllers\Message::class,"sendMessage"])->middleware(\Solle\PcbMessage\Http\Middleware\SendMessageMiddleware::class);
    Route::get("/sse",[\Solle\PcbMessage\Http\Controllers\Message::class,"sseMessages"]);
    Route::post("/send/image",[\Solle\PcbMessage\Http\Controllers\Message::class,"sendImage"])->middleware(\Solle\PcbMessage\Http\Middleware\SendMessageMiddleware::class);
    Route::post("/send/file",[\Solle\PcbMessage\Http\Controllers\Message::class,"sendFile"])->middleware(\Solle\PcbMessage\Http\Middleware\SendMessageMiddleware::class);


    Route::get("/{session}",[\Solle\PcbMessage\Http\Controllers\Message::class,"getMessages"])->middleware(\Solle\PcbMessage\Http\Middleware\SendMessageMiddleware::class);
});
